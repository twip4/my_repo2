public class ClientPlanningCollections 
{
    public static void main(String[] args) {
        PlanningCollections planning = new PlanningCollections();
        System.out.println(planning);

        planning.ajout(new Reservation("plage", new Date(6, 6, 2021), new PlageHoraire(new Horaire(16, 0), new Horaire(20, 20))));
        planning.ajout(new Reservation("plage", new Date(5, 6, 2021), new PlageHoraire(new Horaire(16, 0), new Horaire(20, 20))));
        planning.ajout(new Reservation("plage", new Date(7, 3, 2021), new PlageHoraire(new Horaire(16, 0), new Horaire(20, 20))));

        System.out.println(planning);
    }
}
