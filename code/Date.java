//package modele;

import java.util.Calendar;

public class Date 
{
    protected int jour;
    protected int mois;
    protected int annee;

    //Constructeur
    public Date(){;}

    public Date(int pJour, int pMois, int pAnnee)
    {
        jour = pJour;
        mois = pMois;
        annee = pAnnee;
    }

    public String toString() 
    {
        return jour + "/" + mois + "/" + annee;
    }

    public static boolean estBisextile(int pAnnee) 
    {
        return  ((pAnnee % 400 == 0) ||
                (pAnnee % 4 == 0 && pAnnee % 100 != 0));
    }

    public static int dernierJour(int pMois, int pAnnee) 
    {
        switch (pMois) {
            case 2:
                if (Date.estBisextile(pAnnee))
                {
                    return 29;
                }
                else
                {
                    return 28;
                }
            case 4: case 6: case 9: case 11: 
                return 30;

            default:
                return 31;
        }
    }

    public boolean estValide() 
    {
        return  annee > 1582 &&
                mois >= 1 && mois <= 12 &&
                jour >= 1 && jour <= dernierJour(mois, annee);
    }

    public static Date lireDate()
    {
        System.out.println("Saississez le jour : ");
        int nJour = Clavier.lireInt();
        System.out.println("Saississez le mois : ");
        int nMois = Clavier.lireInt();
        System.out.println("Saississez l'année : ");
        int nAnnee = Clavier.lireInt();
        return new Date(nJour, nMois, nAnnee);
    }

    //mutateur
    public void setJour(int argJour)    {this.jour = argJour;}
    public void setMois(int argMois)    {this.mois = argMois;}
    public void setAnnee(int argAnnee)  {this.annee = argAnnee;}

    //accesseur
    public int getJour()    {return this.jour;}
    public int getMois()    {return this.mois;}
    public int getAnnee()   {return this.annee;}


    public int compareTo(Date argDate) 
    {
        /**
         * return 1 si this est plus grande
         * return -1 si this est plus petite
         * return 0 si les dates sont égales
         */
        if (this.annee < argDate.getAnnee())    {return -1;}
        if (this.annee > argDate.getAnnee())    {return 1;}
        if (this.mois > argDate.getMois())      {return -1;}
        if (this.mois < argDate.getMois())      {return 1;}
        if (this.jour < argDate.getJour())      {return -1;}
        if (this.jour > argDate.getJour())      {return 1;}
        return 0;
    }

    public Date dateDuLendemain() 
    {
        Date dateRetour = new Date();
        dateRetour.setMois(this.mois);
        dateRetour.setAnnee(this.annee);
        
        //Jour
        dateRetour.setJour(this.jour += 1);
        if (dateRetour.getJour() > Date.dernierJour(this.mois, this.annee))
        {
            dateRetour.setJour(1);
            //Mois
            dateRetour.setMois(this.mois += 1);
            if (dateRetour.getMois() > 12) 
            {
                dateRetour.setMois(1);
                //année
                dateRetour.setAnnee(this.annee += 1);
            }
        }
        return dateRetour;
    }

    public Date dateDeLaVeille()
    {
        Date dateRetour = new Date();
        dateRetour.setMois(this.mois);
        dateRetour.setAnnee(this.annee);

        dateRetour.setJour(this.jour -= 1);
        if (dateRetour.getJour() < 1)
        {
            dateRetour.setMois(this.mois - 1);
            dateRetour.setJour(Date.dernierJour(dateRetour.getMois(), dateRetour.getAnnee()));
            if (dateRetour.getMois() < 1) 
            {
                dateRetour.setMois(12);
                dateRetour.setAnnee(this.annee -= 1);
            }
        }
        return dateRetour;
    }

    public int numberOfWeek()
    {
        Calendar cal = Calendar.getInstance();
        cal.set(annee, mois-1, jour);
        return cal.get(Calendar.WEEK_OF_YEAR);
    }
}