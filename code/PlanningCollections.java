import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeMap;
import java.util.TreeSet;

public class PlanningCollections 
{
    private ArrayList<Reservation> reservationsArray;
    private TreeSet<Reservation> reservationsTree;
    private TreeMap<Integer, TreeSet<Reservation>> reservationsMap;
    
    public PlanningCollections()
    {
        reservationsArray = new ArrayList<Reservation>();
        reservationsTree = new TreeSet<Reservation>();
        reservationsMap = new TreeMap<Integer, TreeSet<Reservation>>();
    }
    
    public String toString() {
        String retourString = "";
        
        //Ajout de l'ArrayList
        retourString = reservationsArray.toString() + " de taille " + reservationsArray.size() + "\n\n";
        
        //Ajout du TreeSet
        retourString += reservationsTree.toString() + " de taille " + reservationsTree.size() + "\n\n";

        //Ajout du TreeMap
        retourString += reservationsMap.toString() + " de taille " + reservationsMap.size();

        return retourString;
    }

    public boolean ajout(Reservation argReserve) 
    {
        Iterator<Reservation> iterateur = reservationsTree.iterator();
        
        while (iterateur.hasNext()) 
        {
            if (iterateur.next().compareTo(argReserve) == 0)
            {
                return false;
            }
        }
        reservationsArray.add(argReserve);
        reservationsTree.add(argReserve);

        int numberOfWeek = ((DateCalendrier)argReserve.getDate()).numberOfWeek();

        if (reservationsMap.containsKey(numberOfWeek))
        {
            reservationsMap.get(numberOfWeek).add(argReserve);
        }
        else
        {
            reservationsMap.put(numberOfWeek, new TreeSet<Reservation>());
            reservationsMap.get(numberOfWeek).add(argReserve);
        }

        return true;
    }

    public TreeSet<Reservation> getReservations(DateCalendrier argDate) 
    {
        TreeSet<Reservation> retourReservations = new TreeSet<Reservation>();
        for (int i = 0; i < reservationsArray.size(); i++) 
        {
            if (reservationsArray.get(i).getDate().compareTo(argDate) == 0)
            {
                retourReservations.add(reservationsArray.get(i));
            }
        }
        return retourReservations;
    }

    public TreeSet<Reservation> getReservations(String argString) 
    {
        TreeSet<Reservation> retourReservations = new TreeSet<Reservation>();
        for (int i = 0; i < reservationsArray.size(); i++) 
        {
            if (reservationsArray.get(i).getIntitule().compareTo(argString) == 0)
            {
                retourReservations.add(reservationsArray.get(i));
            }
        }
        return retourReservations;
    }


}
