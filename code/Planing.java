import java.util.Arrays;

public class Planing 
{
    private Reservation [] reservations;
    private static final int TAILLE_TABLEAU = 10;

    public Planing() 
    {
        reservations = new Reservation [TAILLE_TABLEAU];
    }

    public boolean ajout(Reservation argReservation) 
    {
        if (argReservation.estValide() && reservations[TAILLE_TABLEAU-1] == null)
        {
            int i;
            for (i = 0; i != reservations.length; i++) 
            {
                if (reservations[i] == null) 
                {
                    reservations[i] = argReservation;
                    return true;
                }
                if (reservations[i].compareTo(argReservation) == 0)
                {
                    //System.out.println("Problème dans le compareTo");
                    return false;
                }
            }
        }
        //System.out.println("reservation invalide ou tableau plein");
        return false;
    }

    public String toString() 
    {
        String chaineRetour = "";
        for (int i = 0; i < reservations.length && reservations[i] != null; i++) 
        {
            chaineRetour += reservations[i] + "\n";
        }
        return chaineRetour;
    }

    public Reservation getReservation(Date parDate)
    {
        for (int i = 0; i < reservations.length; i++) 
        {
            if (reservations[i].getDate().compareTo(parDate) == 0) 
            {
                System.out.println("1");
                return reservations[i];
            }
        }
        return null;
    }

    public Reservation [] getReservations(Date parDate)
    {
        Reservation [] reserve = new Reservation[10];
        int ajout_tab = 0;
        for (int i = 0; i < reservations.length && reservations[i] != null; i++) 
        {
            if (reservations[i].getDate().compareTo(parDate) == 0) 
            {
                reserve[ajout_tab] = reservations[i];
                ajout_tab++;
            }
        }
        return reserve;
    }

    public int plusAncienneReservation(int argDebut, int argFin) 
    {
        int reservation_retour = argDebut;
        for (int i = argDebut + 1; i < argFin && reservations[i] != null; i++) 
        {
            if (reservations[reservation_retour].compareTo(reservations[i]) == 1)
            {
                reservation_retour = i;
            }
        }
        return reservation_retour;
    }

    public void triParSelection()
    {
        for (int i = 0; i < reservations.length && reservations[i] != null; i++) 
        {
            int indicePlusPetit = i;
            Reservation tmp;
            for (int j = i+1; j < reservations.length && reservations[j] != null; j++) 
            {
                if (reservations[indicePlusPetit].compareTo(reservations[j]) > 0)
                {
                    indicePlusPetit = j;
                    tmp = reservations[i];
                    reservations[i] = reservations[indicePlusPetit];
                    reservations[indicePlusPetit] = tmp;
                }
            }
        }
    }

    public void tri() 
    {
        Arrays.sort(reservations);
    }

}
