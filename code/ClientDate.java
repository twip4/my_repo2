class ClientDate
{
    public static void main(String[] args) 
    {
        Date[] tabDate = new Date[8];

        tabDate[0] = new Date(31, 4, 2022);
        tabDate[1] = new Date(30, 2, 2022);
        tabDate[2] = new Date(31, 13, 2021);
        tabDate[3] = new Date(29, 2, 2000);
        tabDate[4] = new Date(29, 2, 2060);
        tabDate[5] = new Date(29, 2, 2100);
        tabDate[6] = new Date(29, 2, 2020);
        tabDate[7] = new Date(29, 2, 2013);
        
        for (int i = 0; i < tabDate.length; i++)
        {
            System.out.println(tabDate[i]);
            System.out.println(tabDate[i].estValide());
            System.out.println(tabDate[i].dateDeLaVeille());
            System.out.println(tabDate[i].dateDuLendemain());
            System.out.println("");
        }
    }

}