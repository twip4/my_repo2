public class ClientReservation 
{
    public static void main(String[] args) 
    {
        Reservation reservation1 = new Reservation("Musculation intensive", new Date(16, 5, 2022), new PlageHoraire(new Horaire(8, 0), new Horaire(9, 45)));
        Reservation reservation2 = new Reservation("Médecin", new Date(17, 5, 2022), new PlageHoraire(new Horaire(13, 0), new Horaire(15, 45)));
        Reservation reservation3 = new Reservation("Cours", new Date(16, 5, 2022), new PlageHoraire(new Horaire(19, 0), new Horaire(19, 45)));
        
        
        System.out.println(reservation1);
        System.out.println(reservation2);
        System.out.println(reservation3);

        System.out.println(reservation1.compareTo(reservation2));
        System.out.println(reservation2.compareTo(reservation1));
        System.out.println(reservation1.compareTo(reservation3));
        System.out.println(reservation3.compareTo(reservation3));


    }
}
