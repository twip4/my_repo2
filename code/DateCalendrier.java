//package modele;

import java.util.Calendar;

public class DateCalendrier extends Date implements ContantesCalendrier, Comparable<Date>
{
    private int jourSemaine;

    public int getJourSemaine() {
        return jourSemaine;
    }

    public DateCalendrier()
    {
        Calendar today = Calendar.getInstance();
        annee = today.get(Calendar.YEAR);
        mois = today.get(Calendar.MONTH) + 1;
        jour = today.get(Calendar.DAY_OF_MONTH);
        int dayOfWeek = today.get(Calendar.DAY_OF_WEEK);

        if (dayOfWeek != 1)
        {
            jourSemaine = dayOfWeek - 1;
        }
        else
        {
            jourSemaine = 7;
        }
    }

    public DateCalendrier(int parJour, int parMois, int parAnnee)
    {
        super(parJour, parMois, parAnnee);
        Calendar date = Calendar.getInstance();
        date.set(parAnnee, parMois-1, parJour);
        int dayOfWeek = date.get(Calendar.DAY_OF_WEEK);

        if (dayOfWeek != 1)
        {
            jourSemaine = dayOfWeek - 1;
        }
        else
        {
            jourSemaine = 7;
        }
    }

    public DateCalendrier dateDuLendemain()
    {
        Date date = super.dateDuLendemain();
        DateCalendrier dateCalendrierRetour = new DateCalendrier(date.jour, date.mois, date.annee);
        return dateCalendrierRetour;
    }

    public DateCalendrier dateDeLaVeille() 
    {
        Date date = super.dateDeLaVeille();
        DateCalendrier dateCalendrierRetour = new DateCalendrier(date.jour, date.mois, date.annee);
        return dateCalendrierRetour;
    }

    public String toString() 
    {
        return "" + JOUR_SEMAINE[jourSemaine - 1] + " " + jour + " " + MOIS_ANNEE[mois - 1] + " " + annee;
    }

    @Override
    public int numberOfWeek() {
        // TODO Auto-generated method stub
        return super.numberOfWeek();
    }

}