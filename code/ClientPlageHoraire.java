public class ClientPlageHoraire
{
    public static void main(String[] args) 
    {
        Horaire horaireDebut = new Horaire(16, 15);
        Horaire horaireFin = new Horaire(17, 15);

        PlageHoraire plage1 = new PlageHoraire(horaireDebut, horaireFin);
        PlageHoraire plage2 = new PlageHoraire(new Horaire(16, 30), new Horaire(18, 30));
        PlageHoraire plage3 = new PlageHoraire(new Horaire(16, 0), new Horaire(16, 15));


        System.out.println(plage1);
        System.out.println(plage2);
        System.out.println(plage3);
        System.out.println(plage1.compareTo(plage2));
        System.out.println(plage2.compareTo(plage1));
        System.out.println(plage1.compareTo(plage3));
        System.out.println(plage3.compareTo(plage1));

        // System.out.println(plage1);
    }
}