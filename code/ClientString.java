public class ClientString
{
    public static void main(String[] args) 
    {
        String a = "t'es le best";
        String b = "tu est le plus beau";

        System.out.println(a.compareTo(b));
        System.out.println(b.compareTo(a));
        System.out.println(a.compareTo(a));

        String z = "zzzzzzzzz";
        String v = "zzbaaaaaaa";

        System.out.println(z.compareTo(v));
        System.out.println(v.compareTo(z));
    }
}